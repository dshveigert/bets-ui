import React, {Component} from 'react';
import {Container, Row, Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

class Home extends Component {
  render() {
    return <Container>
      <Row>
        <Col><h1>Home</h1></Col>
      </Row>
      <Row>
        <Col>
          <Card>
            <Card.Body>
              <Card.Title>All bets</Card.Title>
              <Card.Text>
                Refreshing through sockets: 1 bet in 1/30 sec.
              </Card.Text>
              <Card.Link as={Link} to="/faster-bets">FasterBets component</Card.Link>
            </Card.Body>
          </Card>
        </Col>
        <Col>
          <Card>
            <Card.Body>
              <Card.Title>All bets (batch)</Card.Title>
              <Card.Text>
                Refreshing through sockets: batch of 30 bets in 1 sec.
              </Card.Text>
              <Card.Link as={Link} to="/bets">Bets component</Card.Link>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  };
}

export default Home;
