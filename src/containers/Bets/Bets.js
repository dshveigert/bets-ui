import React, {Component} from 'react';
import {Container, Row, Col, Table, Spinner} from 'react-bootstrap';
import {connect} from 'react-redux';

import {BetsModal} from '../../components/Bets/BetsModal/BetsModal';
import BetsList from '../../components/Bets/BetsList/BetsList';
import Counter from '../../components/Bets/BetsCounter/BetsCounter';
import {getBetsList, betsModalHandler, socketBetsListUnsubscribe} from '../../store/bets/bets.actions';

import './bets.scss';

class Bets extends Component {

  componentDidMount () {
    this.props.getBets();
  }

  componentWillUnmount() {
    this.props.socketDisconnect();
  }

  render() {
    const {betsLoading, modal, modalHandler} = this.props;

    return <Container className='bets-all'>
      <Row>
        <Col>
          <h1>All bets (batch) {betsLoading && <Spinner animation="grow" />}</h1>
          <p>Refreshing through sockets: batch of 30 bets in 1 sec.</p>
        </Col>
        <Col className='my-auto text-right'><Counter counter='bets' /></Col>
      </Row>
      <Row>
        <Col>
          <Table striped bordered hover className='bets-list'>
            <thead>
            <tr>
              <th className='id'>ID</th>
              <th className='username'>User</th>
              <th>Time</th>
              <th>Bet</th>
              <th>Multiplier</th>
              <th>Game</th>
              <th>Outcome</th>
              <th className='text-right'>Profit</th>
            </tr>
            </thead>
            <tbody>
              <BetsList />
            </tbody>
          </Table>
        </Col>
      </Row>
      {modal && <BetsModal item={modal} handleClose={() => modalHandler(null)} />}
    </Container>
  };
}
const mapStateToProps = state => {
  return {
    modal: state.bets.modal
  };
}
const mapDispatchToProps = dispatch => {
  return {
    getBets: () => dispatch(getBetsList()),
    modalHandler: (item) => dispatch(betsModalHandler(item)),
    socketDisconnect: () => dispatch(socketBetsListUnsubscribe())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Bets);
