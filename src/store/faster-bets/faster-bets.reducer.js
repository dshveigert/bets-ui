import {updateObject} from '../utility';
import * as actions from './faster-bets.const';

const initialState = {
  bets: [],
  counter: 0,
  error: false,
  paused: false,
  modal: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.FBETS_LIST_RESOLVE:
      return updateObject( state, {bets: action.payload});
    case actions.FBETS_LIST_REJECT:
      return updateObject( state, {error: action.payload});
    case actions.FBETS_COUNTER_UPDATE:
      return updateObject( state, {counter: action.payload});
    case actions.FBETS_COUNTER_PAUSED:
      return updateObject( state, {paused: action.payload});
    case actions.FBETS_MODAL_SHOW:
      return updateObject( state, {modal: action.payload});

    default:
      return state;
  }
};

export default reducer;
