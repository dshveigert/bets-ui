import {socketApi} from '../../api/Socket.api';
import * as actions from './faster-bets.const';

const listLimit = 30;
const socketName = 'faster-bets';

export const socketBetsList = () => {
  return (dispatch, state) => {
    socketApi.socket.on(socketName, (data) => {
      const {fbets: {paused, bets}} = state();
      if (data && !paused) {
        const betsList = [data, ...bets];
        betsList.splice(listLimit);
        dispatch({type: actions.FBETS_LIST_RESOLVE, payload: betsList});
        dispatch(updateCounter(data.id));
      }
    });
  }
}

export const socketBetsListUnsubscribe = () => {
  return () => {
    socketApi.socket.off(socketName);
  }
}

export const socketBetsListPaused = () => {
  return (dispatch, state) => {
    const {fbets: {paused}} = state();
    dispatch({type: actions.FBETS_COUNTER_PAUSED, payload: !paused})
  }
}

const updateCounter = (data) => {
  return dispatch => {
    dispatch({type: actions.FBETS_COUNTER_UPDATE, payload: data});
  }
}

export const betsModalToggler = (bet) => {
  return (dispatch) => {
    dispatch({type: actions.FBETS_MODAL_SHOW, payload: bet})
  }
}

export const betsModalHandler = (bet) => {
  return (dispatch) => {
    dispatch({type: actions.FBETS_MODAL_SHOW, payload: bet});
    dispatch(socketBetsListPaused());
  }
}
