import {updateObject} from '../utility';
import * as actions from './bets.const';

const initialState = {
  bets: [],
  counter: 0,
  loading: false,
  error: false,
  paused: false,
  modal: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.BETS_LIST_LOADING:
      return updateObject( state, {loading: action.payload});
    case actions.BETS_LIST_RESOLVE:
      return updateObject( state, {bets: action.payload});
    case actions.BETS_LIST_REJECT:
      return updateObject( state, {error: action.payload});
    case actions.BETS_COUNTER_UPDATE:
      return updateObject( state, {counter: action.payload});
    case actions.BETS_COUNTER_PAUSED:
      return updateObject( state, {paused: action.payload});
    case actions.BETS_MODAL_SHOW:
      return updateObject( state, {modal: action.payload});

    default:
      return state;
  }
};

export default reducer;
