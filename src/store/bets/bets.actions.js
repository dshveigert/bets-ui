import {betsApi} from '../../api/Bets.api';
import {socketApi} from '../../api/Socket.api';
import * as actions from './bets.const';

const socketName = 'bets';

export const getBetsList = () => {
  return dispatch => {
    dispatch({type: actions.BETS_LIST_LOADING, payload: true});
    betsApi.getList()
        .then(response => {
          dispatch({type: actions.BETS_LIST_LOADING, payload: false});
          if (response) {
            updateListEmitter(dispatch, response.data.data);
          }
          return response;
        })
        .catch(error => {
          dispatch({type: actions.BETS_LIST_REJECT});
          dispatch({type: actions.BETS_LIST_LOADING, payload: false});
          return error
        })
        .finally(() => {
          dispatch(socketBetsList());
        })
  }
};

export const socketBetsList = () => {
  return (dispatch, state) => {
    socketApi.socket.on(socketName, (data) => {
      if (data) {
        updateListEmitter(dispatch, data, state);
      }
    });
  }
}

export const socketBetsListUnsubscribe = () => {
  return () => {
    socketApi.socket.off(socketName);
  }
}

export const socketBetsListPaused = () => {
  return (dispatch, state) => {
    const {bets: {paused}} = state();
    dispatch({type: actions.BETS_COUNTER_PAUSED, payload: !paused})
  }
}

const updateListEmitter = (dispatch, data, state) => {
  if (Array.isArray(data) && data.length > 0 && (!state || !state().bets.paused)) {
    const rev = data.reverse();
    dispatch({type: actions.BETS_LIST_RESOLVE, payload: rev});
    dispatch(updateCounter(rev[0].id));
  }
}

const updateCounter = (data) => {
  return dispatch => {
    dispatch({type: actions.BETS_COUNTER_UPDATE, payload: data});
  }
}

export const betsModalToggler = (bet) => {
  return (dispatch) => {
    dispatch({type: actions.BETS_MODAL_SHOW, payload: bet})
  }
}

export const betsModalHandler = (bet) => {
  return (dispatch) => {
    dispatch({type: actions.BETS_MODAL_SHOW, payload: bet});
    dispatch(socketBetsListPaused());
  }
}
