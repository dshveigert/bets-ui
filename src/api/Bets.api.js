import {Service} from './Services.api';

class BetsApi extends Service {
  getList() {
    return this.get(`/bets`);
  }
}

export const betsApi = new BetsApi();
