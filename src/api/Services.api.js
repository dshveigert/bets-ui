import axios from 'axios';

export class Service {
  constructor() {
    const headers = {
      'Content-Type': 'application/json;charset=UTF-8'
    };
    const host = process.env.NODE_ENV === 'production' ? 'http://37.194.18.29:37000/api' : 'http://localhost:7000/api';
    this.service = axios.create({headers, baseURL: host});
  }

  get(path, params) {
    return this.service.get(`${path}`, params);
  }
}

export const service = new Service();
