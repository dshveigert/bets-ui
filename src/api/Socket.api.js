import io from 'socket.io-client';

export class SocketApi {
  constructor() {
    const host = process.env.NODE_ENV === 'production' ? 'http://37.194.18.29:37000/socket-bets' : 'http://localhost:7000/socket-bets';

    this.socket = io(host, {
      autoConnect: true
    });
  }
}

export const socketApi = new SocketApi();
