import React from 'react';
import {timeFormat} from '../../../utils';


const BetsListItem = (props) => {
  const {item, modalHandler} = props;

  return <tr className='bets-list-item' onClick={() => {modalHandler(item)}}>
    <td className='id'>{item.id}</td>
    <td className='username'>{item.user}</td>
    <td>{timeFormat(item.time)}</td>
    <td>{item.bet}</td>
    <td>{item.multiplier}</td>
    <td>{item.game}</td>
    <td>{item.out}</td>
    <td className='text-right'>{item.profit}</td>
  </tr>
};
export default BetsListItem;
