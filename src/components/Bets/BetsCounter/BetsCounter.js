import React, {Component} from 'react';
import {connect} from 'react-redux';

class Counter extends Component {
  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return (this.props.counter === 'bets' && this.props.counters.bets !== nextProps.counters.bets)  || (this.props.counter === 'fbets' && this.props.counters.fbets !== nextProps.counters.fbets);
  }

  render() {
    const {counters, counter} = this.props;
    const value = counter === 'bets' ? counters.bets : counters.fbets;
    return <span>Counter: {value} bets</span>
  }
}

const mapStateToProps = (state) => {
  return {
    counters: {bets: state.bets.counter, fbets: state.fbets.counter}
  }
}

export default connect(mapStateToProps)(Counter);
