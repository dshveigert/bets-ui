import React, {PureComponent} from 'react';
import {connect} from 'react-redux';

import BetsListItem from '../BetsListItem/BetsListItem';
import {betsModalHandler} from '../../../store/faster-bets/faster-bets.actions';

import './bets-list.scss';

class FasterBetsList extends PureComponent {
  render() {
    const {bets, modalHandler} = this.props;

    return bets.map((item, i) => <BetsListItem item={item} key={i} className='bets-list-item' modalHandler={(data) => modalHandler(data)} />);
  };
}

const mapStateToProps = state => {
  return {
    bets: state.fbets.bets
  }
};

const mapDispatchToProps = dispatch => {
  return {
    modalHandler: (item) => dispatch(betsModalHandler(item))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FasterBetsList);
