import React, {Component} from 'react';
import {Modal, Button, Table} from 'react-bootstrap';
import {timeFormat} from '../../../utils';

export class BetsModal extends Component {

  render() {
    const {item, handleClose} = this.props;

    return <Modal show={!false} onHide={handleClose} animation={false}>
      <Modal.Header closeButton>
        <Modal.Title># {item.id}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Table striped>
          <tbody>
          <tr>
            <td>User</td><td>{item.user}</td>
          </tr>
          <tr>
            <td>Time</td><td>{timeFormat(item.time)}</td>
          </tr>
          <tr>
            <td>Bet</td><td>{item.bet}</td>
          </tr>
          <tr>
            <td>Multiplier</td><td>{item.multiplier}</td>
          </tr>
          <tr>
            <td>Game</td><td>{item.game}</td>
          </tr>
          <tr>
            <td>Outcome</td><td>{item.out}</td>
          </tr>
          <tr>
            <td>Profit</td><td>{item.profit}</td>
          </tr>
          </tbody>
        </Table>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  };
}
