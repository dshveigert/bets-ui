import React, {Component} from 'react';
import {Container, Nav, Navbar} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export class Navigation extends Component {
  render() {
    return <Navbar expand="lg" variant="light" bg="light">
      <Container>
        <Nav className="mr-auto">
          <Nav.Link as={Link} to="/" >Home</Nav.Link>
          <Nav.Link as={Link} to="/faster-bets" >FasterBets</Nav.Link>
          <Nav.Link as={Link} to="/bets" >Bets</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  }
}
