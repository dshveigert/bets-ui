import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from './containers/Home/Home';
import Bets from './containers/Bets/Bets';
import {Navigation} from './components/Navigation/Navigation';

import FasterBets from './containers/FasterBets/FasterBets';
import './App.scss';

function App() {
  return (
    <div className="App">
      <Navigation />
      <Switch>
        <Route path='/bets' component={Bets} />
        <Route path='/faster-bets' component={FasterBets} />
        <Route exact path='/' component={Home} />
      </Switch>
    </div>
  );
}

export default App;
