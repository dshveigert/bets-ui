This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Bets list
Render bets in a package and one at a time (30 bets per sec and 1 bet in 1/30 sec). Receive data by sockets.

## Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the app in the development mode.<br />
Open [http://localhost:4200](http://localhost:4200) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

